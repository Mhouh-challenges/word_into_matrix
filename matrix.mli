type 'a t
val from: 'a array array -> 'a t option
val ( .%() ): 'a t -> int * int -> 'a
val width: 'a t -> int
val height: 'a t -> int
val positions: _ t -> (int * int) Seq.t
