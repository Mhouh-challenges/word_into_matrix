type 'a t = 'a array array
let is_matrix arrays =
  not (Array.exists
         (fun subarray -> Array.length subarray <> (Array.length arrays.(0)))
         arrays)
let from arrays =
  if is_matrix arrays
  then Some arrays
  else None
let ( .%() ) m (i, j) = m.(i).(j)
let width m = Array.length (m.(0))
let height m = Array.length m
let positions m =
  Seq.(
    init (height m)
      (fun i ->
         Seq.init (width m) (fun j ->
             (i, j))
      )
    |> concat)
