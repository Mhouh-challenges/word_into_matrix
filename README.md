# Path for a word in a matrix
## The challenge
given a matrix of symbols, and a word composed of symbols, determine a path of adjacent cells in the matrix that form the word, if such a path exists.
For example, given the matrix
```
AB
CD
```
the word "ABACD" can be obtained by starting on A, then going right, left, down, right.
## My take
### Moving through the matrix
We can see matrices as a graph, where adjacent cells are connected nodes.
```
      ----->
  A   <-----   B

/|\ |        /|\ |
 |  |         |  |
 | \|/        | \|/

  C   ----->   D
      <-----
```
given any position (i, j) in the matrix, neighbours can be found with:
```ocaml
let around x = [ x - 1; x + 1 ] |> List.to_seq

let within ~width ~height (i, j) = i >= 0 && j >= 0 && i < height && j < width

let neighbours (i, j) ~width ~height =
  Seq.(
    append
      (product (return i) (around j))
      (product (around i) (return j))
    |> filter (within ~width ~height)
  )
```
Note we avoid diagonals and positions that are out of the bounds of the matrix.
### Path searching
Stepping through our matrix gives an infinite tree of possible paths (except for matrix that are 1x1 or less).
We will walk through this tree, but keeping only the branches that match our desired path. This is pretty similiar to a [Depth-First traversal](https://en.wikipedia.org/wiki/Tree_traversal#Depth-first_search).
Here, we will also keep track of the path we took.
```ocaml
let rec accepting_paths_from position word matrix =
  match Seq.uncons word with
  | None -> Seq.return [ position ]
  | Some (symbol, rest) ->
    let width, height = Matrix.(width matrix, height matrix) in
    let neighbours = neighbours position ~width ~height in
    let reachable_neighbours =
      Seq.filter (fun pos -> matrix.%(pos) = symbol) neighbours in
    let accepting_paths = 
      Seq.flat_map
        (fun pos -> accepting_paths_from pos rest matrix)
        reachable_neighbours in
    Seq.map (List.cons position) accepting_paths
```
Then, we can use this function, starting on every cell of the matrix that contains the first symbol of our target word:
```
let accepting_paths word matrix = 
  match Seq.uncons word with
  | None -> Seq.return []
  | Some (symbol, rest) ->
    Matrix.positions matrix
    |> Seq.flat_map
      (fun pos ->
         if matrix.%(pos) = symbol
         then accepting_paths_from pos rest matrix
         else Seq.empty)
```
Note that, with this approach, we could find all possible paths.
As `Seq.t`'s evaluation is lazy, retrieving only one path means we skip all the following, but we could get all of them if wanted.
## Final note
- Example is found in `main.ml`.
- Run with `dune exec ./main.ml`
- Edit the file to try with different matrices/words!
- Remarks, comments are welcome!
