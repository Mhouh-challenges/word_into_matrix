let ( .%() ) = Matrix.( .%() )
let ( >> ) f g x = g (f x)

let around x = [ x - 1; x + 1 ] |> List.to_seq

let within ~width ~height (i, j) = i >= 0 && j >= 0 && i < height && j < width

let neighbours (i, j) ~width ~height =
  Seq.(
    append
      (product (return i) (around j))
      (product (around i) (return j))
    |> filter (within ~width ~height)
  )

let rec accepting_paths_from position word matrix =
  match Seq.uncons word with
  | None -> Seq.return [ position ]
  | Some (symbol, rest) ->
    let width, height = Matrix.(width matrix, height matrix) in
    let neighbours = neighbours position ~width ~height in
    let reachable_neighbours =
      Seq.filter (fun pos -> matrix.%(pos) = symbol) neighbours in
    let accepting_paths =
      Seq.flat_map
        (fun pos -> accepting_paths_from pos rest matrix)
        reachable_neighbours in
    Seq.map (List.cons position) accepting_paths

let accepting_paths word matrix =
  match Seq.uncons word with
  | None -> Seq.return []
  | Some (symbol, rest) ->
    Matrix.positions matrix
    |> Seq.flat_map
      (fun pos ->
         if matrix.%(pos) = symbol
         then accepting_paths_from pos rest matrix
         else Seq.empty)

let first = Seq.uncons >> Option.map fst

let accepts word matrix = accepting_paths word matrix |> first |> Option.is_some
