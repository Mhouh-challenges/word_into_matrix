open Definitions

type symbol = A | B | C | D | E | F | G

let () = ignore (A, B, C, D, E, F, G)

let m = Matrix.from
    [|
      [| A; B; C; |];
      [| A; A; D; |];
      [| E; F; G; |];
    |]

let word = [ A; B; A; A; A; F; E; ] |> List.to_seq

let path = Option.bind m (accepting_paths word >> first)

let symbol_to_char = function
  | A -> 'A'
  | B -> 'B'
  | C -> 'C'
  | D -> 'D'
  | E -> 'E'
  | F -> 'F'
  | G -> 'G'

let string_of_matrix m =
  let (width, height) = Matrix.(width m, height m) in
  let bytes = Bytes.make ((width * 2 + 1) * height) '\n' in
  Seq.iter
    (fun (i, j) ->
       Bytes.set bytes (j * 2 + i * (width * 2 + 1)) (symbol_to_char (m.%(i, j)));
       Bytes.set bytes (j * 2 + 1 + i * (width * 2 + 1)) ' ')
    (Matrix.positions m);
  String.of_bytes bytes

let string_of_word w = String.of_seq (Seq.map symbol_to_char w)

let print_path out path =
  let rec print_path out = function
    | [] -> ()
    | (i, j) :: rest ->
      Printf.fprintf out "\n(%d, %d); %a" i j print_path rest in
  Printf.fprintf out "[%a]" print_path path

let _ =
  let m = try Option.get m with _ ->
    Printf.eprintf "Failed to create the matrix.\n%s"
      "Make sure all the lines have the same number of elements.";
    exit ~-1 in
  Option.iter
    (fun path ->
       Printf.printf
         "The matrix:\n%saccepts the word '%s' with the following path:\n%a\n"
         (string_of_matrix m)
         (string_of_word word)
         print_path path;
    ) path;
  if Option.is_none path then print_endline "no path was found..."
